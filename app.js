const urlUsers = 'https://www.mockachino.com/a71b232c-218e-4d/users';
let users = getUsers(urlUsers);
let pageNumber = 1;
let cantElements = 10;

users.then(users => {
    const allUsers = users.results
    let usersToShow = allUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements)
    showUser(usersToShow)

    const pages = generatePages(users.results);
    const pagesContainer = document.getElementById('pages');
    pagesContainer.innerHTML = pages;

    document.querySelectorAll('.pageButton').forEach(button => {
        button.addEventListener('click', (e) => {
            pageNumber = parseInt(e.target.firstChild.data)
            usersToShow = allUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements)
            showUser(usersToShow)
        })
    })

    document.getElementById('prevButton').addEventListener('click', () =>{
        if (pageNumber = pageNumber - 1 ) {
            usersToShow = allUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements)
            showUser(usersToShow)
        }
    })

    document.getElementById('nextButton').addEventListener('click', () =>{
        if (pageNumber = pageNumber + 1 ) {
            usersToShow = allUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements)
            showUser(usersToShow)
        }
    })

    document.querySelector('.searchInput').addEventListener('input', (e) => {
        let search;
        if (e.target.value === '') {
            showUser(users.results.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements));
        } else {
            search = users?.results.filter(usrs => usrs.name.first.toLowerCase().includes(e.target.value.toLowerCase()));
            console.log(search);
            showUser(search);
        }
    })
})

function selectSort() {
    let options = document.getElementById('sortAlpha');
    let sortSelected = options.value;
    let sortedUsers;
    users.then(users => {
        if (sortSelected === 'a-z') {
            sortedUsers = users.results.sort(function (a, b) {
                if (a.name.first > b.name.first) {
                    return 1;
                }
                if (a.name.first < b.name.first) {
                    return -1;
                }
                return 0;
            })
            sortedUsers = sortedUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements);
            showUser(sortedUsers);
        }

        if (sortSelected === 'z-a') {
            sortedUsers = users.results.sort(function (a, b) {
                if (a.name.first > b.name.first) {
                    return -1;
                }
                if (a.name.first < b.name.first) {
                    return 1;
                }
                return 0;
            })
            sortedUsers = sortedUsers.slice((cantElements * pageNumber) - cantElements, pageNumber * cantElements);
            showUser(sortedUsers);
        }

        // if (sortSelected === 'none') {
        //     showUser(users.results);
        // }

    })

    console.log(sortSelected)
}


async function getUsers(url) {
    const response = await fetch(url);
    let data = await response.json();
    return data;
}

function showUser(array) {
    let card = '';
    for (let r of array) {
        card += `
            <div class="userContainer">
            <img src="${r.picture.large}">
            <div class="name">${r.name.first} ${r.name.last}</div>
            <div class="info"><img class="mail" src="mail.png">${r.email}</div>
            <div class="info"><img class="location" src="map.png">${r.location.city} ${r.location.state}</div>
            <hr style="width:50%", size="1", color=grey>
            <div class="bottom">
              <div class="bottomInfo"><img class="clock" src="time-left.png">${r.registered.age}Y ago</div>
            </div>
            <div><img class='heart' src="heart.png"></div>
            </div>`
    }
    document.querySelector('.containerFlex').innerHTML = card;
}

function generatePages(array) {
    if (array) {
        const cantPages = Math.ceil(array.length / cantElements)
        let pagesHTML = ''

        for (i = 1; i <= cantPages; i++) {
            pagesHTML += `
            <button class='pageButton'>${i}</button>
            `
        }
        return pagesHTML
    }
}
